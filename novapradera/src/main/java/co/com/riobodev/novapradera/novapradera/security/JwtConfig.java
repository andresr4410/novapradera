package co.com.riobodev.novapradera.novapradera.security;

public class JwtConfig {
	public static final String LLAVE_SECRETA = "alguna.clave.secreta.12345678";
	
	public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\r\n"
			+ "MIIEowIBAAKCAQEA0uJv5sdq5RPZOgHoQGydORtsBHfZdZUCS4NnfJ4NkoxfsSVz\r\n"
			+ "Gc9TQSBWb1IiYr3t/TGxvbXvM8kjMMYr7EVOqDxXQnWBgUZNIBItccxzqL9/VGBL\r\n"
			+ "bvFvxgilhUgP/9xt77zXa3TqUHZX2xf3aIE7wkNvpyeF7BxNdlYDS5ZGGLpQIJAE\r\n"
			+ "59MEE5GRVTmOeejrdsFt/78INsIGPMUO8Q4q/P5++jfXKPrwicVxGXmqLxhqhkGT\r\n"
			+ "0DUTpdFGUQwXqRRgK0nEUJWpUOpUSPqDiD9R3XwF+Kvzy66PFu+cMPg1Wl+JfkjT\r\n"
			+ "PWl36c8Id+NcqIn32L4HVGZuvDdDyFF9zeBeqwIDAQABAoIBAEJtmZo0jvOCARdI\r\n"
			+ "qoRAsnXesXPm3NUxDvwCdILItrXHCg2fIlaq/CqzqQZ/9Nztvx3WnYqI2MSTQ0aF\r\n"
			+ "5igS3OgZudLxeM3j+z9y+mWyZWxuhglqG9jirLbQ2nLVdFTAdtDhx2nN9lFKNfqJ\r\n"
			+ "3iFJNC3JWE5rIMBzWeNAzODugT4gP8Fj+KHHED7AylICPfx2jvHtUtKX/tPAAnZu\r\n"
			+ "TMHHzm9P6dAzEv1AJat3eOKb2czoW0OB2PaPPTIKDVlVbxAyvb72ZwjAp9VDxL0k\r\n"
			+ "xLde4vwz1I3HC5G4BJsfD3ql9NZ7dyCT9unaO/rzyEvoVVjdK+dhqWhDROT7Rubj\r\n"
			+ "ZVmCLGECgYEA8n2gKn4OECYM0Fub4cfqk/rUb3O1dyUntkj4QJugbl0crcJSNZ7m\r\n"
			+ "qgt3y3TQVgeKwrj0YX6+yg5JUoi3BlzgYIUcH6lm8QjII0szXF9JXo7Ll1b/ZzOv\r\n"
			+ "bYfkLlP45ZfSIOVi0fbtI33KNUUG6niawMDrPGTHuXSOA9Uy8T1SjVUCgYEA3qIM\r\n"
			+ "JrrsXKqXcNTz1a7pUUcYb+Pyu1Vf5wMeKiXA3lDV8r+ZZs4+gC+rMYv/7hy2k14H\r\n"
			+ "WZk8Gz8/V2R/OZrAZmetS8Z8V0jVO4ufjAAxMlBwojLRFvneNzI98TC1gky98yMn\r\n"
			+ "FxP5zVa+AGQCec+sfWxHDUfa3KlTr7o+/IHHO/8CgYAKM3i6cOUcjs05MrFjFSrs\r\n"
			+ "IMAxFBgNNBXwmC9BYIqmOeqL8MHOmamwe50FhRuCkvymZmt5coCKNYGmJDQ2409Z\r\n"
			+ "ICBnwJOhDjKFMZOujh+jB7TO4A0MhWMa432Y6VVuKy3ezmeS16VwOcl45jd9Q6eW\r\n"
			+ "9kavGrpANgQHaz+iYGcJpQKBgHo7okxJaKhgCRGr+cwiYnt7e1FDZ+uurh7ZuWAO\r\n"
			+ "plvpKe5Df+fP6MTcZzOm1/dV0HAMuiBZX4TeLtq+lJN+AZC374+hBHevx1MQD6MA\r\n"
			+ "IAKt44lKOT5SJc1MSnH3+k/17nxlmnqzPv2cWGlCf5F9kgiywDfscZ+hYA8lRJvC\r\n"
			+ "bS7lAoGBAKYGXLm9usvm1g9jWZedu+ogl8XFDSi08US8RnSfItGYNWC5mIKfNVnS\r\n"
			+ "5fIODNqr7ncpeX+28jaCQygox8QijyI4GaVLZHqI7RYqEnfiGjnzjQAnpSGakjME\r\n"
			+ "KzUp+Yvl+AmFALm/MzFLor69j0rixQPpy4hzpRgc0wMb9XBCZYJq\r\n"
			+ "-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\r\n"
			+ "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0uJv5sdq5RPZOgHoQGyd\r\n"
			+ "ORtsBHfZdZUCS4NnfJ4NkoxfsSVzGc9TQSBWb1IiYr3t/TGxvbXvM8kjMMYr7EVO\r\n"
			+ "qDxXQnWBgUZNIBItccxzqL9/VGBLbvFvxgilhUgP/9xt77zXa3TqUHZX2xf3aIE7\r\n"
			+ "wkNvpyeF7BxNdlYDS5ZGGLpQIJAE59MEE5GRVTmOeejrdsFt/78INsIGPMUO8Q4q\r\n"
			+ "/P5++jfXKPrwicVxGXmqLxhqhkGT0DUTpdFGUQwXqRRgK0nEUJWpUOpUSPqDiD9R\r\n"
			+ "3XwF+Kvzy66PFu+cMPg1Wl+JfkjTPWl36c8Id+NcqIn32L4HVGZuvDdDyFF9zeBe\r\n"
			+ "qwIDAQAB\r\n"
			+ "-----END PUBLIC KEY-----";

}