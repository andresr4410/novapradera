package co.com.riobodev.novapradera.novapradera.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.riobodev.novapradera.novapradera.domain.Evento;


@Repository
public interface IEventoRepository extends JpaRepository<Evento, Long>{

}
