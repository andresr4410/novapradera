package co.com.riobodev.novapradera.novapradera.repository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.riobodev.novapradera.novapradera.domain.Usuario;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario,Long >{

	public Usuario findByUsername(String username);
	
	@Query("select u from Usuario u where u.username=?1")
	public Usuario findByUsername2(String username);
	
}
