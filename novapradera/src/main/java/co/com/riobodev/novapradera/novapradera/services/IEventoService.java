package co.com.riobodev.novapradera.novapradera.services;

import java.util.List;

import co.com.riobodev.novapradera.novapradera.domain.Evento;

public interface IEventoService {

	List<Evento> findAll();
	
	Evento findById(Long id);

	void delete(Long id) throws Exception;
}
