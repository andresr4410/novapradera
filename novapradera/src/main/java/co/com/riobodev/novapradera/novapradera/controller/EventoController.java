package co.com.riobodev.novapradera.novapradera.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.riobodev.novapradera.novapradera.domain.Evento;
import co.com.riobodev.novapradera.novapradera.services.IEventoService;

@RestController
@RequestMapping("/eventos")
public class EventoController {
	
	private final IEventoService eventoService;

    public EventoController(IEventoService eventoService) {
        this.eventoService = eventoService;
    }
    
    @GetMapping("/findAll")
    public ResponseEntity<List<Evento>> findAllEventos() {
        List<Evento> eventos = eventoService.findAll();
        return ResponseEntity.ok(eventos);
    }
   
   
	@GetMapping("/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		
		Evento evento = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			evento = eventoService.findById(id);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(evento == null) {
			response.put("mensaje", "El evento con ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Evento>(evento, HttpStatus.OK);
	}
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> eliminar(@PathVariable Long id) {
        try {
            eventoService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    
}
