package co.com.riobodev.novapradera.novapradera.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import co.com.riobodev.novapradera.novapradera.domain.Evento;
import co.com.riobodev.novapradera.novapradera.repository.IEventoRepository;


@Service
public class EventoServiceImpl implements IEventoService{
	
	private IEventoRepository eventoRepository; 
	
	public EventoServiceImpl(IEventoRepository eventoRepository) {
        this.eventoRepository = eventoRepository;
    }

	@Override
	public List<Evento> findAll() {
		return eventoRepository.findAll();
	}

	@Override
	public Evento findById(Long id) {
		return eventoRepository.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) throws Exception {
        Optional<Evento> eventoExistente = eventoRepository.findById(id);
        if (eventoExistente.isPresent()) {
        	eventoRepository.delete(eventoExistente.get());
        } else {
            throw new Exception("No se encontró la categoría con el ID proporcionado");
        }
    }

}
