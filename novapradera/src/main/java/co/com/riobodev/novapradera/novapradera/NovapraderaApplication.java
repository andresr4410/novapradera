package co.com.riobodev.novapradera.novapradera;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class NovapraderaApplication implements CommandLineRunner {
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(NovapraderaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String password = "andres";
		
		for (int i = 0; i < 4; i++) {
			String passwordBcryt = passwordEncoder.encode(password);
			System.out.println(passwordBcryt);
		}
		
	}
	
	
	
}
