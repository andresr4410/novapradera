package co.com.riobodev.novapradera.novapradera.services;

import co.com.riobodev.novapradera.novapradera.domain.Usuario;

public interface IUsuarioService {

	public Usuario findByUsername(String username);
}
